# 基于SpringBoot+SSM的外卖系统

### 介绍
基于SpringBoot+SSM的外卖系统。
用户端接口:分类、微信用户、菜品/套餐浏览、订单与支付、购物车、地址簿

管理端接口:分类、用户管理、菜品/套餐管理、订单管理、数据统计、店铺状态操作、工作台

涉及的服务：阿里云OSS（文件上传）、百度GPS定位服务（外卖派送测距）、微信小程序登录、支付

涉及的技术：aop、mysql、spring-task，websocket，httpclient、redis、spring-cache等

### 效果图
![输入图片说明](imageimage.png)
![输入图片说明](image/image.png)

### 使用方式
源码为SpringBoot后端代码，需要配合管理端前端+微信小程序用户端前端。
依赖：
redis
mysql
java jdk17 
阿里云OSS服务，百度GPS服务，微信小程序接口服务都需要设置成自己的🔑。
管理端前端+微信小程序前端等更详细资料见:
https://www.bilibili.com/video/BV1TP411v7v6/。
### 联系方式
 **Email:zylu2000@163.com** 
 **Thank you！** 