package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/admin/common")
@Api(tags = "文件上传")
public class CommonController {
    @Autowired
    private AliOssUtil aliOssUtil;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file)  {
        try {
            log.info("文件上传");
            String originalFilename= file.getOriginalFilename();
            int index= originalFilename.lastIndexOf('.');
            String name=originalFilename.substring(index);
            String finalName= name +UUID.randomUUID().toString();
            String upload = aliOssUtil.upload(file.getBytes(), finalName);
            log.info("文件上传成功");
            return Result.success(upload);
        } catch (IOException e) {
            log.info("文件上传错误");
        }
        return Result.error(MessageConstant.UPLOAD_FAILED);
    }
}
