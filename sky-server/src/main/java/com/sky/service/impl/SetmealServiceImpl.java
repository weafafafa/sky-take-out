package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealOverViewVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private DishMapper dishMapper;
    @Transactional
    @Override
    public void saveWithDish(SetmealDTO setmealDTO){
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.insert(setmeal);
        Long setmealId=setmeal.getId();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmealId);
        }
        setmealDishMapper.insertBatch(setmealDishes);
    }

    @Override
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO){
        PageHelper.startPage(setmealPageQueryDTO.getPage(), setmealPageQueryDTO.getPageSize());
         Page<SetmealVO> setmealVOS=  setmealMapper.pageQuery(setmealPageQueryDTO);
        PageResult pageResult = new PageResult();
        pageResult.setTotal(setmealVOS.getTotal());
        pageResult.setRecords(setmealVOS.getResult());
        return pageResult;
    }

    @Override
    @Transactional
    public void deleteBatch(List<Long> ids){
        Setmeal setmeal;
        //1.起售中的不能删除
        for (Long id : ids) {
            setmeal= setmealMapper.getById(id);
            if(setmeal.getStatus()== StatusConstant.ENABLE){
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }
        //2.删除套餐以及套餐菜品表对应信息
        for(Long id:ids){
            setmealMapper.deleteId(id);
            setmealDishMapper.deleteBySetmealId(id);
        }
    }


    @Override
    public  SetmealVO getById(Long id){
        //setmel与category表联系
        SetmealVO setmealVO = setmealMapper.listWithCategory(id);
        //setmeal与setmeal_dish表联系
        List<SetmealDish> setmealDishes= setmealDishMapper.getSetmealDishById(id);
        setmealVO.setSetmealDishes(setmealDishes);
        return setmealVO;
    }
    @Override
    public void startOrStop(Integer status, Long id){
        //1.如果起售,判断起售套餐中菜品是否有停售的
        if(status == StatusConstant.ENABLE){
        List<Dish> dishList = dishMapper.getBySetmealId(id);
        if(dishList!=null && dishList.size()>0){
            for (Dish dish : dishList) {
                if(dish.getStatus()==StatusConstant.DISABLE){
                    throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
                }
            }
         }
        }
        //2.起售或停售套餐
        Setmeal setmeal=new Setmeal();
        setmeal.setId(id);
        setmeal.setStatus(status);
        setmealMapper.update(setmeal);
    }

    @Transactional
    @Override
    public  void update(SetmealDTO setmealDTO){
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        //1.修改setmeal表
        setmealMapper.update(setmeal);

        //2.修改setmeal_dish表
        Long id=setmealDTO.getId();
        setmealDishMapper.deleteBySetmealId(id);
        for (SetmealDish setmealDish : setmealDishes) {
               setmealDish.setSetmealId(id);
        }
        setmealDishMapper.insertBatch(setmealDishes);
    }

    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    public List<Setmeal> list(Setmeal setmeal) {
        List<Setmeal> list = setmealMapper.list(setmeal);
        return list;
    }

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {
        return setmealMapper.getDishItemBySetmealId(id);
    }
}
