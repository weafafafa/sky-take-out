package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import com.sky.vo.UserLoginVO;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private WeChatProperties weChatProperties;
    @Override
    public User login(UserLoginDTO userLoginDTO) {
        String code= userLoginDTO.getCode();
        Map<String,String>paramsMap=new HashMap<>();
        paramsMap.put("appid", weChatProperties.getAppid());
        paramsMap.put("secret",weChatProperties.getSecret());
        paramsMap.put("js_code",code);
        paramsMap.put("grant_type","authorization_code");
        String result= HttpClientUtil.doGet("https://api.weixin.qq.com/sns/jscode2session",paramsMap);
        JSONObject jsonObject = JSON.parseObject(result);
        String openid= jsonObject.getString("openid");

        if(openid==null){
            throw  new LoginFailedException(MessageConstant.LOGIN_FAILED);
        }
        User user = userMapper.getByOpenId(openid);
        if(user==null){
            user=new User();
            user.setCreateTime(LocalDateTime.now());
            user.setOpenid(openid);
            userMapper.save(user);
        }
        return user;
    }
}
