package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */

    @Select("select count(id) from dish where category_id = #{categoryId}")
    Integer countByCategoryId(Long categoryId);

    @AutoFill(OperationType.INSERT)
    void save(Dish dish);

    Page<DishVO> pageQuery(DishPageQueryDTO dishPageQueryDTO);



    @Select("select * from dish where id=#{id}")
     Dish getById(Long id);

    @Delete("delete  from dish where id =#{id}")
    void deleteById(Long id);


    void  deleteByIds(List<Long> ids);

    @AutoFill(OperationType.UPDATE)
    void updateDish(Dish dish);


    List<Dish> list(Dish dish);

    @Select("select d.* from dish d left join setmeal_dish sd on d.id=sd.dish_id where sd.setmeal_id=#{id}")
    List<Dish> getBySetmealId(Long id);


    /**
     * 根据条件统计菜品数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}
